Import this project in eclipse the follow this steps to run the tests (Eclipse Mars RC2):

   - go to Run->Run Configurations...
   - on Left side double click on JUnit, new configuration will be created
   - select "Run all tests in the selected project, package or source folder" under Test tab
   - select JUnit 4 under Test Runner
   - click apply and then Run