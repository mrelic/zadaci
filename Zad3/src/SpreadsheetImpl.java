
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

enum ValueType {
    STRING,
    INTEGER,
    FORMULA
}

public class SpreadsheetImpl {

   private int columns;
   private int rows;
   private FileOutputStream fileOut;
   private static HSSFWorkbook wb;
   private static FileInputStream fileIn;
   private static HSSFSheet spreadsheet;
   private static HSSFRow row;
   private static HSSFCell cell;

   public int getRows() {
      return this.rows;
   }
   
   public void setRows(int rows) {
      this.rows = rows;
   }
   
   public int getColumns() {
      return this.columns;
   }
   
   public void setColumns(int columns) {
      this.columns = columns;
   }

   public SpreadsheetImpl(int rows, int columns) throws FileNotFoundException {
      this.setRows(rows);
      this.setColumns(columns);
      
      try {
         fileOut = new FileOutputStream("workbook.xls");
         wb = new HSSFWorkbook();
         spreadsheet = wb.createSheet("Sheet1");
         for (int i=0; i < this.rows; i++) {
            row = spreadsheet.createRow((short) i);
            for (int j=0; j < this.columns; j++) {
               cell=row.createCell(j);
               cell.setCellValue("");
               cell.setCellType(HSSFCell.CELL_TYPE_BLANK);
               
            }
         }

         wb.write(fileOut);
         fileOut.close();
      } catch (IOException e) {
         e.printStackTrace();
      }
      
      
   }

   public Object get(int i, int j) {
      if (i>=rows || j>=columns) {
         throw new IndexOutOfBoundsException();
      }
      try {
         fileIn = new FileInputStream("workbook.xls");
         spreadsheet = wb.getSheetAt(0);
         row = spreadsheet.getRow(i);
         cell = row.getCell(j);
         fileIn.close();
         switch (cell.getCellType()) {
         case HSSFCell.CELL_TYPE_STRING:
             return cell.getRichStringCellValue().toString();
         case HSSFCell.CELL_TYPE_NUMERIC:
             DecimalFormat format = new DecimalFormat("0.#");
             return format.format(cell.getNumericCellValue());
         case HSSFCell.CELL_TYPE_BOOLEAN:
             return cell.getBooleanCellValue();
         case HSSFCell.CELL_TYPE_FORMULA:
             return cell.getRichStringCellValue().toString();
         default:
             return "";
         }
      } catch ( IOException e) {
         e.printStackTrace();
      }
      return null;
   }

   public void put(int i, int j, String string) {
      if (i>=rows || j>=columns) {
         throw new IndexOutOfBoundsException();
      }
      try {
         fileOut = new FileOutputStream("workbook.xls");
         spreadsheet = wb.getSheetAt(0);
         row = spreadsheet.getRow(i);
         cell = row.getCell(j);
         if(string.startsWith("=")) {
            cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cell.setCellValue(string);
         } else if(string.trim().matches("-?\\d+")) {
            cell.setCellValue(Integer.valueOf(string.trim()));
         } else {
            cell.setCellValue(string);
         }
         
         wb.write(fileOut);
         fileOut.close();
      } catch ( IOException e) {
         e.printStackTrace();
      }

   }
   
   public ValueType getValueType(int i, int j) {
      if (i>=rows || j>=columns) {
         throw new IndexOutOfBoundsException();
      }
      try {
         fileOut = new FileOutputStream("workbook.xls");
         spreadsheet = wb.getSheetAt(0);
         row = spreadsheet.getRow(i);
         cell = row.getCell(j);
         wb.write(fileOut);
         fileOut.close();
         switch (cell.getCellType()) {
         case HSSFCell.CELL_TYPE_STRING:
             return ValueType.STRING;
         case HSSFCell.CELL_TYPE_NUMERIC:
             return ValueType.INTEGER;
         case HSSFCell.CELL_TYPE_BOOLEAN:
             return ValueType.INTEGER;
         case HSSFCell.CELL_TYPE_FORMULA:
             return ValueType.FORMULA;
         default:
             return ValueType.INTEGER;
         }

      } catch ( IOException e) {
         e.printStackTrace();
      }

      return null;
   }
   
}
