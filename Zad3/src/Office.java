import java.io.FileNotFoundException;

public class Office {
   public static SpreadsheetImpl newSpreadsheet(int rows, int columns) {
      SpreadsheetImpl sheet = null;
      try {
         sheet = new SpreadsheetImpl(rows, columns);
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }
      
      return sheet;
   }

}
