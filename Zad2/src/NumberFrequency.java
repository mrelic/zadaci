/*
 *  2. Zadatak
 *  
 *  Potrebno je napisati program koji iz komandne linije prima jedan parametar, 
 *  koji je string te sadr�i samo brojeve odvojene �_� npr �1_33_12�.
 *  Program na ekran treba ispisati frekvenciju pojavljivanja brojeva 
 *  npr za 10_32_10_5_11_10_5_32 potrebno je ispasti 10_3,32_2,5_2,11_1.
 *  
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.List;

public class NumberFrequency {
   public static void main(String[] args) {
      /* Exit if number of parameters are not valid */
      if (args.length != 1) {
         System.out.println("Proper Usage is: java NumberFrequency 10_32_10_5_11_10_5_32");
         System.exit(0);
      }
      
      /* Hash where frequency of numbers will be stored */
      HashMap<String, Integer> numberFreqHash = new HashMap<>();
      /* We split input argument */
      String[] inputNumbers = args[0].split("_");
      /* Calculating frequency */
      for (String i : inputNumbers) {
         if (numberFreqHash.containsKey(i)) {
            numberFreqHash.put(i, numberFreqHash.get(i) + 1);
         }
         else {
            numberFreqHash.put(i, 1);
         }
      }
      
      /* Multihash where frequency of numbers will be stored by their frequency */
      Map<Integer, List<String>> numberFreqMultiHash = new HashMap<Integer, List<String>>();

      /* Sorting map by frequency */
      for (Map.Entry<String, Integer> entry : numberFreqHash.entrySet()) {
         if (numberFreqMultiHash.containsKey(entry.getValue())) {
            numberFreqMultiHash.get(entry.getValue()).add(entry.getKey());
         }
         else {
            numberFreqMultiHash.put(entry.getValue(), new ArrayList<String>());
            numberFreqMultiHash.get(entry.getValue()).add(entry.getKey());
         }
      }

      /* Lets sort multihash by frequency in descending order */
      Map<Integer, List<String>> treeMap = new TreeMap<Integer, List<String>>(numberFreqMultiHash);
      NavigableMap<Integer, List<String>> nmap=((TreeMap<Integer, List<String>>) treeMap).descendingMap();
      
      /* And finally lets printout frequency of numbers */
      for (Map.Entry<Integer, List<String>> entry : nmap.entrySet()) {
         Collections.sort(entry.getValue());
         for (String str : entry.getValue()) {
            /* Print in format: number_frequency */
            System.out.print(str + "_" + entry.getKey());
            /* This if is here so last comma(,) is not print it */
            if (entry.getKey() != nmap.lastKey() || entry.getValue().get(entry.getValue().size() - 1) != str) {
               System.out.print(",");
            }
         }
      }
   }
}
