If you import this project in eclipse, run it with:

   - go to Run->Run configurations...
   - in there, for NumberFrequency application, under Arguments tab
     in Program arguments add arguments that you want to pass to program
     e.g. 10_32_10_5_11_10_5_32
   - now press Apply button and then Run
   - after setting input arguments as above, just press Ctrl+F11 
     to start it again.
   - if you want to pass another arguments, change it as mentioned above
   
If you want to run it in terminal, run it with:

   - first go into bin directory, if NumberFrequency.class file exist, run it with:
      > java NumberFrequency 10_32_10_5_11_10_5_32
     
   - if .class file doesn't exist, go to src directory, then first compile .java with:
      > javac NumberFrequency.java
     then run the program like before:
      > java NumberFrequency 10_32_10_5_11_10_5_32

   - if you run program with just:
     > java NumberFrequency
     short help will be print it out
       