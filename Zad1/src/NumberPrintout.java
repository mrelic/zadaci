/*
 *  1. Zadatak
 *  
 *  Potrebno je napisati program koji ispisuje brojeve od 1 do 100. 
 *  Za brojeve djeljive s 3 potrebno je ispisati SV, za brojeve 
 *  djeljive s 5 potrebno je ispisati Group. Za brojeve djeljive 
 *  s 3 i s 5 potrebno je ispisati SVGroup.
 * 
 */

public class NumberPrintout {
   /* How many numbers to printout */
   private int howManyNumbersToPrint = 0;
   
   /* Set how many numbers to printout */
   public void setHowManyNumbersToPrint(int n) {
      howManyNumbersToPrint = n;
   }
   
   /* Get how many numbers to printout */
   public int getHowManyNumbersToPrint() {
      return howManyNumbersToPrint;
   }
   
   /* Print numbers with additional text */
   public void printThoseNumbers(NumberPrintout numberPrintout) {
      /* Lets print those numbers */
      for (int i=1; i <= numberPrintout.getHowManyNumbersToPrint(); i++) {
         System.out.print(i + " ");
         /* If number is divided only by 3, printout "SV" */
         if (i%3==0) {
            System.out.print("SV");
         }
         /* If number is divided only by 5, printout "Group" */
         if (i%5==0) {
            System.out.print("Group");
         }
         System.out.println();
      }
   }
   
  public static void main(String[] args) {
      /* Create object of this class */
      NumberPrintout numberPrintout = new NumberPrintout();
      
      /* Set 100 numbers to printout */
      numberPrintout.setHowManyNumbersToPrint(100);
      
      /* Print numbers with additional text */
      numberPrintout.printThoseNumbers(numberPrintout);
   }
}


/*
 * This is short version :) 
 *
public class NumberPrintout {
   public static void main(String[] args) {
      for (int i=1; i <= 100; i++) {
         // Use of ternary operator in printout function
         System.out.println((i%15==0) ? "SVGroup" : ((i%3==0) ? "SV" :((i%5==0) ? "Group" : i)));
      }
   }
}
*/