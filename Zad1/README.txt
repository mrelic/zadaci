If you import this project in eclipse, run it with:

   - Run button on menu (or Run->Run)
   - or just press Ctrl+F11 (shortkey for runing the project)
   
If you want to run it in terminal, run it with:

   - first go into bin directory, if NumberPrintout.class file exist, run it with:
      > java NumberPrintout
     
   - if .class file doesn't exist, go to src directory, then first compile .java with:
      > javac NumberPrintout.java
     then run the program like before:
      > java NumberPrintout
       